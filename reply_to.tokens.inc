<?php

/**
 * @file
 * Builds placeholder replacement tokens for reply-to global 'site' token.
 */

/**
 * Implements hook_token_info().
 */
function reply_to_token_info() {
  $info['tokens']['site']['reply-to'] = array(
    'name' => t("Reply-to"),
    'description' => t("The Reply-to address for this site."),
  );

  return $info;
}

/**
 * Implements hook_tokens().
 */
function reply_to_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();
  if ($type == 'site' && !empty($data['site'])) {
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'reply-to':
          $replacements[$original] = variable_get('site_reply_to', '');
          break;
      }
    }
  }

  return $replacements;
}
