INTRODUCTION
------------
This module adds a Reply-to header to all outgoing mail using the drupal mail
system. It also supplies a token [site:reply-to] to use.

REQUIREMENTS
------------
Drupal 7.x

INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------

*Customize the reply-to address in Administration >> Configuration >>
Site Information

CONTACT
-------

Current maintainer:
* Matt Garrison (mgarrison) - https://www.drupal.org/user/2312886

This project has been sponsored by:
* Upside Collective
  We are a versatile design and marketing firm located in Albany, NY.
  With focus on graphic design, web development, motion graphics and video.
  production, we are committed to elevating your business from concept to
  completion. We love to share our knowledge with our clients and look forward
  to creating lasting relationships. Whether big or small, your company matters.

SUPPORT
-------
Please use the issue queue for filing bugs or features with this module at:
